package com.olivares.retrofitapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by molivars on 23/07/2018.
 */

public interface IRetro {
    @GET("usersFake")
//indicamos el metodo y el endpoint
    Call<List<ApiResp>> getUsersGet();//Recuerda que debes colocar como recibiremos esos datos,en este caso una lista de objs


    @POST("usersFake")
//Metodo post
    Call<List<ApiResp>> getUsersPost();

    //https://androidtutorials.herokuapp.com/findUser?id=1
    @GET("findUser")
    Call<ApiResp> findUserGet(@Query("id") int idUser);//Recuerda que el valor @Query(valor) debe ser igual a como lo espera el servicio

    //https://androidtutorials.herokuapp.com/findUserPost
    @FormUrlEncoded//Recuerda que si envias parametros en post,es necesario el encode
    @POST("findUserPost")
    Call<String> findUserPost(@Field("name") String nombre);//el nombre del parametro debe ser igual al que e servicio espera

}
