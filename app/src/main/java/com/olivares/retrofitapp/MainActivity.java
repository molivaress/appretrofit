package com.olivares.retrofitapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    String TAG = MainActivity.class.getName();
    final String url = "https://androidtutorials.herokuapp.com/";
    ListView users;
    public List<String> usersList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        users = (ListView) findViewById(R.id.users);
        final ArrayAdapter<String> arrayAdapter;
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, usersList);
        users.setAdapter(arrayAdapter);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)//Indicamos la url del servicio
                .addConverterFactory(GsonConverterFactory.create())//Agregue la fábrica del convertidor para la serialización y la deserialización de objetos.
                .build();//Cree la instancia de Retrofit utilizando los valores configurados.

        IRetro service = retrofit.create(IRetro.class);
        Call<List<ApiResp>> call = service.getUsersGet();
        call.enqueue(new Callback<List<ApiResp>>() {
            @Override
            public void onResponse(Call<List<ApiResp>> call, Response<List<ApiResp>> response) {
                if (response.code() == 200) {
                    for (int x = 0; x < 10; x++) {
                        for (ApiResp ar : response.body()) {
                            Log.d(TAG, ar.getName());
                            usersList.add(ar.getName() + " " + ar.getLastName());
                        }
                    }
                    arrayAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<ApiResp>> call, Throwable t) {
                call.cancel();
                Log.d(TAG, t.toString());
            }
        });

         /*Call<ApiResp> call = service.findUserGet(3);
        call.enqueue(new Callback<ApiResp>() {
            @Override
            public void onResponse(Call<ApiResp> call, Response<ApiResp> response) {
                if (response.code() == 200) {
                    Log.d(TAG, response.body().getLastName());
                }
            }

            @Override
            public void onFailure(Call<ApiResp> call, Throwable t) {
                call.cancel();
                Log.d(TAG, t.toString());
            }
        });*/

    }


}
